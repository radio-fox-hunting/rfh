# Radio Fox Hunting

## O projekcie
Radio Fox Hunting (Polowanie na lisa) jest to zabawa łącząca biegi na orientację z technologiami. Głównym jej założeniem jest szukanie punktów kontrolnych wyposażonych w nadajniki, kierując się odbiornikiem, przechwytującym wysyłane z nich (nadajników) sygnały.

Urządzenia nadają różne sygnały, dzięki czemu gracz może wywnioskować, na które urządzenie aktualnie się kieruje. Odbiera on dokładny sygnał jeśli jest skierowany mniej więcej w stronę urządzenia, oraz słabszy sygnał, gdy urządzenie którego szuka znajduje się w zupełnie innym kierunku. Zadaniem gracza jest znalezienie możliwie jak największej liczby punktów kontrolnych, oraz dotarcie do końca trasy w możliwie jak najkrótszym czasie.

## Słowa kluczowe

- **nadajnik** - urządzenie wysyłające sygnały do odbiornika. (faza 2)
- **odbiornik** - urządzenie odbierające sygnały od nadajnika, oraz przekierowujące je do urządzenia. (faza 2)
- **punkt kontrolny** - punkt do którego musi dotrzeć gracz, aby otrzymać punkty.
- **punkt końcowy** - punkt na trasie kończący rozgrywkę danego zawodnika, gdy zostanie przez niego osiągnięty.
- **urządzenie** - dowolne urządzenie z systemem Android.
- **wirtualny punkt kontrolny** - punkt kontrolny oznaczony tylko i wyłącznie na mapach (nie posiadający fizycznego nadajnika).
- **zawodnik/uczestnik/gracz** - osoba w świecie rzeczywistym, posługująca się urządzeniem w celu znalezienia punktów kontrolnych, oraz dotarciu do punktu końcowego.

## Fazy projektu
Projekt podzielono na 2 główne fazy. Spełnienie założeń fazy 1 wiąże się z wykonaniem minimalnych założeń projektowych. Faza 2 jest natomiast rozszerzeniem możliwości projektu o dodatkową - bardziej profesjonalną funkcjonalność.

### Faza 1
W pierwszej fazie projektu założono osiągnięcie minimalnych założenia projektowe.

Do założeń tej fazy należą:

- Stworzenie aplikacji na dowolne urządzenie z systemem Android, posiadającej interfejs graficzny dopasowujący się do dowolnej orientacji ekranu, oraz system pozwalający na tworzenie przez jedną osobę, lub losowanie przez komputer głównych WIRTUALNYCH punktów kontrolnych w obrębie kilku kilometrów. Zadaniem osoby biorącej udział w zabawie będzie przejście przez te punkty, aby zdobyć maksimum punktów. Urządzenie będzie zaliczać dotarcie do punktu, gdy gracz znajdzie się we wcześniej ustalonej odległości od niego.
- Algorytm symulujący działanie urządzenia wskazującego kierunek w którym zawodnik powinien się poruszać, aby dotrzeć do punktu kontrolnego. System ten będzie oparty o GPS, oraz magnetometr. Pozwoli to na symulację urządzenia odbierającego sygnały z radiostacji.
- Strona Internetowa z bazą danych przechowującą wyniki poszczególnych zawodów.

### Faza 2
W fazie drugiej, projekt zostanie rozszerzony o nadajniki i odbiorniki. Zawodnik będzie posiadał przy sobie jeden odbiornik namierzający nadajniki do których powinien dotrzeć. Nadajniki natomiast staną się punktami kontrolnymi.

W fazie tej wirtualne punkty zostaną więc zamienione na punkty z urządzeniami fizycznymi (lub staną się dodatkową funkcjonalnością obok punktów wirtualnych). Komunikacja między urządzeniami będzie odbywać się przy użyciu jednego ze standardów LPWAN posiadających zasięg do przynajmniej 5 kilometrów. Ponadto odbiornik będzie komunikować się z jego urządzeniem zawodnika przy użyciu sieci Bluetooth.

## Technologie
W naszym projekcie wykorzystamy następujące technologie:

- **Flutter** - do napisania aplikacji na urządzenia z systemem Android
- **ASP .NET** - do stworzenia strony Internetowej
- **MySQL** - do stworzenia, oraz korzystania z bazy danych
- **???** - do zaprogramowania odbiorników i nadajników
