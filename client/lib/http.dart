import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import './global.dart' as global;

var get_url = 'https://rfh1000020210216120302.azurewebsites.net';
var post_url = 'rfh1000020210216120302.azurewebsites.net';

// get your id
Future<int> login(String username) async
{
  final response = await http.get(get_url + "/api/login");

  if (response.statusCode == 200) {
    return convert.jsonDecode(response.body);
  } else {
    throw Exception('Failed to load id');
  }
}

Future<http.Response> newGame(lat, lng) async
{
  return http.post(
      Uri.https(post_url, 'api/new'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: convert.jsonEncode(<String, dynamic>{
        "Player_id": global.id,
        "Player_loc": {
          "X": lat,
          "Y": lng
        }
      })
  );
}

Future<List> getPlayersLocations() async
{
  final response = await http.get(get_url + "/api/game/");

  if (response.statusCode == 200) {
    return convert.jsonDecode(response.body);
  } else {
    throw Exception('Failed to load players location');
  }
}

Future<http.Response> postYourLocation(lat, lng) {
  return http.post(
    Uri.https(post_url, 'api/game'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: convert.jsonEncode(<String, dynamic>{
      "Player_id": global.id,
      "Player_name": global.username,
      "Player_loc": {
        "X": lat,
        "Y": lng
      },
      "Player_radius": 0.0
    }
  ));
}
