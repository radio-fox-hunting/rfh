import 'dart:math';

import 'package:flutter/material.dart';

import 'global.dart' as global;

double getZoom() {
  double zoomLevel = 11;
  if (global.radius > 0) {
    double radiusElevated = global.radius + global.radius / 2;
    double scale = radiusElevated / 500;
    zoomLevel = 16 - log(scale) / log(2);
  }
  zoomLevel = num.parse(zoomLevel.toStringAsFixed(2)) - 0.5;
  return zoomLevel;
}

double getPercentageWidth(context, orientation, percentage) {
  return MediaQuery.of(context).size.width * percentage;
}

double getPercentageHeight(context, orientation, percentage) {
  return MediaQuery.of(context).size.height * percentage;
}
