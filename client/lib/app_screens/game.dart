import 'dart:async';

import 'package:client/common.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../http.dart' as http;
import '../global.dart' as global;


class GameScreen extends StatefulWidget {
  @override
  State<GameScreen> createState() => _GameScreenState();
}


class _GameScreenState extends State<GameScreen> {

  static final double maxRadius = 500;

  GoogleMapController _controller;
  Location _location = Location();

  static final CameraPosition _gdanskPosition = CameraPosition(
      target: LatLng(54.372158, 18.638306),
      zoom: 14,
  );

  void createCircle(data)
  {
    circles.add(new Circle(
      circleId: CircleId(circles.length.toString()),
      center: data["center"],
      radius: data["radius"],
      fillColor: Color(0x260000FF),
      strokeColor: Color(0x800000FF),
      strokeWidth: 0,
    ));
  }

  void updateCircles(data)
  {
    circles.clear();
    for (int i = 0; i < data.length; i++) {
      createCircle(data[i]);
    }
    this.setState(() {});
  }

  Set<Circle> circles = Set.from([Circle(
    circleId: CircleId("0"),
    center: LatLng(54.372158, 18.638306),
    radius: 0,
    fillColor: Color(0x260000FF),
    strokeColor: Color(0x800000FF),
    strokeWidth: 0,
  )]);

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
        builder: (context, orientation) {
          return Scaffold(
              appBar: AppBar(
                title: Text(
                  "The Game",
                  style: TextStyle(
                    fontFamily: "Lato",
                    letterSpacing: 2,
                  ),
                ),
              ),
              body: GestureDetector(
                  onTap: () {},
                  child: Material(
                    color: Colors.lightBlue,
                    child: maps(context),
                  )
              )
          );
        }
    );
  }

  Widget maps(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: GoogleMap(
        mapType: MapType.normal,
        myLocationEnabled: true,
        myLocationButtonEnabled: true,
        initialCameraPosition: _gdanskPosition,
        onMapCreated: (GoogleMapController controller) {
          _controller = controller;
          _location.onLocationChanged.listen((event) {
            _controller.animateCamera(
              CameraUpdate.newCameraPosition(
                CameraPosition(
                  target: LatLng(event.latitude, event.longitude),
                  zoom: getZoom(),
                )
              )
            );

            if (global.futureLocations == null) {
              Timer(Duration(milliseconds: 30), () {
                http.postYourLocation(event.latitude, event.longitude);
                global.futureLocations = http.getPlayersLocations();
              });
            } else {
              global.futureLocations.then((locationsList) => {
                locationsList.forEach((element) {
                  if (element["Player_radius"] <= 10) {
                    finishGame(context, element["Player_name"]);
                  }

                  global.circlesData.add({
                    "center": LatLng(
                        element["Player_loc"]["X"],
                        element["Player_loc"]["Y"]
                    ),
                    "radius": element["Player_radius"]
                  });

                  if (element["Player_id"] == global.id) {
                    global.radius = element["Player_radius"];
                  }
                }),
                updateCircles(global.circlesData),
                global.circlesData.clear(),

                global.futureLocations = null,
              });
            }
          });
        },
        onCameraMove: null,
        circles: circles,
      ),
    );
  }

  void finishGame(BuildContext context, winnerName) {
    void leaveGame() {
      Navigator.pop(context); // close text dialog
      Navigator.pop(context); // close maps
      Navigator.pop(context);
    }

    var alertDialog = AlertDialog(
      title: Text("Game Over"),
      content: Container(
        height: 102,
        child: Column(
          children: [
            Center(
              child: Text(
                "$winnerName won the game",
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'Lato',
                )
              ),
            ),
            Container(
              height: 32,
            ),
            RaisedButton(
              color: Colors.orangeAccent,
              padding: EdgeInsets.all(6),
              child: Text(
                'OK',
                style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                  fontFamily: 'Lato',
                  letterSpacing: 4,
                ),
              ),
              onPressed: () => leaveGame(),
            )
          ],
        ),
      ),
    );

    showDialog(
      context: context,
      builder: (BuildContext context) => alertDialog,
    );
  }
}
