import 'package:flutter/material.dart';

import '../common.dart';
import '../global.dart' as global;
import '../http.dart' as http;
import './main_menu.dart';

class HomeScreen extends StatelessWidget {

  TextEditingController usernameController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Material(
        color: Colors.lightBlue,
        child: OrientationBuilder(
          builder: (context, orientation) {
            return Center(
                child: Container(
                    child: Column(
                      children: [
                        Spacer(),
                        Text(
                            "Radio Fox Hunting",
                            textDirection: TextDirection.ltr,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: getPercentageWidth(context, orientation, 0.12),
                              fontFamily: 'DancingScript',
                            )
                        ),
                        Container(
                          height: getPercentageHeight(context, orientation, 0.08),
                        ),
                        Container(
                          width: getPercentageWidth(context, orientation, 0.5),
                          child: TextFormField(
                            controller: usernameController,
                            decoration: InputDecoration(
                              labelText: 'Enter your username',
                              labelStyle: TextStyle(
                                fontSize: getPercentageWidth(context, orientation, 0.05),
                                fontFamily: 'Lato',
                                color: Colors.white,
                              ),
                            ),
                            style: TextStyle(
                              fontSize: getPercentageWidth(context, orientation, 0.06),
                              fontFamily: 'Lato',
                              height: 1.0,
                              color: Colors.white,
                            ),
                            cursorColor: Colors.white,
                            cursorWidth: 1.5,
                          ),
                        ),
                        Container(
                          height: 40.0,
                        ),
                        Container(
                          width: getPercentageWidth(context, orientation, 0.40),
                          height: getPercentageHeight(context,  orientation, 0.08),
                          child: RaisedButton(
                            color: Colors.orangeAccent,
                            padding: EdgeInsets.all(6),
                            elevation: 10.0,
                            child: Text(
                              'Play',
                              style: TextStyle(
                                fontSize: getPercentageHeight(context, orientation, 0.045),
                                color: Colors.white,
                                fontFamily: 'Lato',
                                letterSpacing: 4,
                              ),
                            ),
                            onPressed: () {
                              if (usernameController.text.isNotEmpty) {
                                global.username = usernameController.text;
                                if (global.futureId == null) {
                                  global.futureId = http.login(global.username);
                                }
                                global.futureId.then((value) => global.id = value);
                                global.futureId.then((value) => {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) {
                                        return MainMenuScreen();
                                      })
                                  )
                                });
                              }
                            },
                          ),
                        ),
                        Spacer(),
                      ],
                    )
                )
            );
          }
        )
      )
    );
  }
}
