import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:location/location.dart';

import '../common.dart';
import './game.dart';
import '../global.dart' as global;
import '../http.dart' as http;

class MainMenuScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
        builder: (context, orientation) {
          return Scaffold(
              appBar: AppBar(
                  title: Text(
                    "Radio Fox Hunting",
                    style: TextStyle(
                      fontFamily: "Lato",
                      letterSpacing: 2,
                    ),
                  )
              ),
              body: Material(
                  color: Colors.lightBlue,
                  child: Center(
                      child: Container(
                          padding: EdgeInsets.only(
                              top: orientation == Orientation.portrait
                                  ? 100
                                  : 10),
                          child: Column(
                            children: [
                              Spacer(),
                              Text(
                                  "Radio Fox Hunting",
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: getPercentageWidth(
                                        context, orientation, 0.12),
                                    fontFamily: 'DancingScript',
                                  )
                              ),
                              Container(
                                height: orientation == Orientation.portrait
                                    ? getPercentageHeight(context, orientation, 0.08)
                                    : getPercentageHeight(context, orientation, 0.04),
                              ),
                              (orientation == Orientation.portrait ?
                                  Column(
                                    children: [
                                      NewGameButton(),
                                      Container(height: getPercentageHeight(context, orientation, 0.014),),
                                      LoadGameButton(),
                                      Container(height: getPercentageHeight(context, orientation, 0.014),),
                                      AboutButton(),
                                      Container(height: getPercentageHeight(context, orientation, 0.014),),
                                      CreditsButton(),
                                      Container(height: getPercentageHeight(context, orientation, 0.014),),
                                      QuitButton(),
                                    ],
                                  ) :
                                  Column(
                                    children: [
                                      Row(
                                        children: [
                                          Spacer(),
                                          NewGameButton(),
                                          Container(width: getPercentageWidth(
                                              context, orientation, 0.020),),
                                          LoadGameButton(),
                                          Spacer(),
                                        ],
                                      ),
                                      Container(height: getPercentageHeight(
                                          context, orientation, 0.01),),
                                      Row(
                                        children: [
                                          Spacer(),
                                          AboutButton(),
                                          Container(width: getPercentageWidth(
                                              context, orientation, 0.020),),
                                          CreditsButton(),
                                          Spacer(),
                                        ],
                                      ),
                                      Container(height: getPercentageHeight(
                                          context, orientation, 0.01),),
                                      QuitButton(),
                                    ],
                                  )
                              ),
                              Container(
                                  height: getPercentageHeight(context, orientation, 0.08)
                              ),
                              Spacer(),
                            ],
                          )
                      )
                  )
              )
          );
        }
    );
  }
}


class NewGameButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
        builder: (context, orientation) {
          return Container(
              width: getPercentageWidth(context, orientation, 0.40),
              height: getPercentageHeight(context,  orientation, 0.08),
              child: RaisedButton(
                color: Colors.orangeAccent,
                padding: EdgeInsets.all(6),
                elevation: 10.0,
                child: Text(
                  'New Game',
                  style: TextStyle(
                    fontSize: getPercentageHeight(context, orientation, 0.05),
                    color: Colors.white,
                    fontFamily: 'Lato',
                    letterSpacing: 4,
                  ),
                ),
                onPressed: () => start(context),
              )
          );
        }
    );
  }

  void start(BuildContext context) {
    Location location = new Location();
    var locationData = location.getLocation();

    locationData.then((location) =>
        http.newGame(location.latitude, location.longitude)
    );
    locationData.then((value) =>
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return GameScreen();
        }))
    );
  }
}


class LoadGameButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
        builder: (context, orientation) {
          return Container(
              width: getPercentageWidth(context, orientation, 0.40),
              height: getPercentageHeight(context,  orientation, 0.08),
              child: RaisedButton(
                color: Colors.orangeAccent,
                padding: EdgeInsets.all(6),
                elevation: 10.0,
                child: Text(
                  'Load Game',
                  style: TextStyle(
                    fontSize: getPercentageHeight(context, orientation, 0.05),
                    color: Colors.white,
                    fontFamily: 'Lato',
                    letterSpacing: 4,
                  ),
                ),
                onPressed: () => start(context),
              )
          );
        }
    );
  }

  void start(BuildContext context) {
    void startGame(String serverId) {
      if (serverId.isNotEmpty) {
        Navigator.pop(context); // close text dialog
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return GameScreen();
        }));
      }
    }

    var usernameController = new TextEditingController();
    var alertDialog = OrientationBuilder(
      builder: (context, orientation) {
        return AlertDialog(
          title: Text(
              "Enter server ID:",
              style: TextStyle(
                fontFamily: "Lato",
                fontSize: getPercentageHeight(context, orientation, 0.03),
              )
          ),
          content: Container(
            height: getPercentageHeight(context, orientation, 0.20),
            child: Column(
              children: [
                Spacer(),
                TextField(
                  controller: usernameController,
                  style: TextStyle(
                    fontFamily: "Lato",
                    fontSize: getPercentageHeight(context, orientation, 0.045)
                  ),
                ),
                Container(
                  height: getPercentageHeight(context, orientation, 0.035),
                ),
                Container(
                  width: getPercentageWidth(context, orientation, 0.40),
                  height: getPercentageHeight(context, orientation, 0.06),
                  child: RaisedButton(
                    color: Colors.orangeAccent,
                    padding: EdgeInsets.all(6),
                    child: Text(
                      'Play',
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                        fontFamily: 'Lato',
                        letterSpacing: 4,
                      ),
                    ),
                    onPressed: () => startGame(usernameController.text),
                  )
                )
              ],
            ),
          ),
        );
      }
    );

    showDialog(
      context: context,
      builder: (BuildContext context) => alertDialog,
    );
  }
}


class AboutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
        builder: (context, orientation) {
          return Container(
              width: getPercentageWidth(context, orientation, 0.40),
              height: getPercentageHeight(context,  orientation, 0.08),
              child: RaisedButton(
                color: Colors.orangeAccent,
                padding: EdgeInsets.all(6),
                elevation: 10.0,
                child: Text(
                  'About',
                  style: TextStyle(
                    fontSize: getPercentageHeight(context, orientation, 0.05),
                    color: Colors.white,
                    fontFamily: 'Lato',
                    letterSpacing: 4,
                  ),
                ),
                onPressed: () => about(context),
              )
          );
        }
    );
  }

  void about(BuildContext context) {
    var alertDialog = OrientationBuilder(
      builder: (context, orientation) {
        return AlertDialog(
          title: Text(
            "About:",
            style: TextStyle(
              fontFamily: "Lato",
              fontSize: getPercentageHeight(context, orientation, 0.035),
            )
          ),
          content: Text(
            "Application for the game Radio Fox Hunting",
            style: TextStyle(
              fontFamily: "Lato",
              fontSize: getPercentageHeight(context, orientation, 0.025)
            ),
          ),
        );
      }
    );

    showDialog(
      context: context,
      builder: (BuildContext context) => alertDialog,
    );
  }
}


class CreditsButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
        builder: (context, orientation) {
          return Container(
              width: getPercentageWidth(context, orientation, 0.40),
              height: getPercentageHeight(context,  orientation, 0.08),
              child: RaisedButton(
                color: Colors.orangeAccent,
                padding: EdgeInsets.all(6),
                elevation: 10.0,
                child: Text(
                  'Credits',
                  style: TextStyle(
                    fontSize: getPercentageHeight(context, orientation, 0.05),
                    color: Colors.white,
                    fontFamily: 'Lato',
                    letterSpacing: 4,
                  ),
                ),
                onPressed: () => credits(context),
              )
          );
        }
    );
  }

  void credits(BuildContext context) {
    var alertDialog = OrientationBuilder(
        builder: (context, orientation) {
          return AlertDialog(
            title: Text(
                "Credits:",
                style: TextStyle(
                  fontFamily: "Lato",
                  fontSize: getPercentageHeight(context, orientation, 0.035),
                )
            ),
            content: Text(
              "Main developer: \nRadosław Wojdak\n\n"
              "Web developer: \nBłażej Tez\n\n"
              "Moral support: \nJakub Jasonek 👍",
              style: TextStyle(
                  fontFamily: "Lato",
                  fontSize: getPercentageHeight(context, orientation, 0.025)
              ),
            ),
          );
        }
    );

    showDialog(
        context: context,
        builder: (BuildContext context) => alertDialog,
    );
  }
}


class QuitButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
        builder: (context, orientation) {
          return Container(
              width: getPercentageWidth(context, orientation, 0.40),
              height: getPercentageHeight(context,  orientation, 0.08),
              child: RaisedButton(
                color: Colors.orangeAccent,
                padding: EdgeInsets.all(6),
                elevation: 10.0,
                child: Text(
                  'Quit',
                  style: TextStyle(
                    fontSize: getPercentageHeight(context, orientation, 0.05),
                    color: Colors.white,
                    fontFamily: 'Lato',
                    letterSpacing: 4,
                  ),
                ),
                onPressed: () => quit(context),
              )
          );
        }
    );
  }

  void quit(BuildContext context) {
    SystemNavigator.pop();
    SystemNavigator.pop();
  }
}
