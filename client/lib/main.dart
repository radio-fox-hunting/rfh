import 'package:flutter/material.dart';
import './app_screens/home.dart';

void main() => runApp(new Application());

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Radio Fox Hunting",
      home: HomeScreen()
    );
  }
}
